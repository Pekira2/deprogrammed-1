﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

	bool stopped = false;
	bool goingUp = true;
	public float speed = 0.5f;
	public float secondsStopped = 3.0f;
	float stoppedTimer = 0;

	// Use this for initialization
	void Start () {
		stopped = (Random.Range(0.0f, 1.0f) > 0);
		goingUp = (Random.Range(0.0f, 1.0f) > 0);
	}
	
	// Update is called once per frame
	void Update () {
		
		if (stopped)
		{
			stoppedTimer += Time.deltaTime;
			if (stoppedTimer > secondsStopped)
			{
				stopped = false;
				stoppedTimer = 0;	
			}
		}
		else if (goingUp)
			transform.position = new Vector3 (transform.position.x, transform.position.y+speed, transform.position.z);
		else if (!goingUp)
			transform.position = new Vector3 (transform.position.x, transform.position.y-speed, transform.position.z);
	}

    void OnTriggerEnter2D(Collider2D other)
    {   
    	if (other.tag == "FloorStopper")
    	{
    		stopped = true;
    	}
    	else if (other.tag == "ElevatorFlip")
    	{	
    		goingUp = !goingUp;
    	}
    }
}
