using UnityEngine;
using System.Collections;

public class RangedState : IEnemyState {

	private Enemy enemy;
	private float shootDuration = 0.5f;
	private float nextShootTime = 0;


    public void Execute(){

    	if (enemy.Target != null)
    	{
    		Ranged();
            enemy.Move();

			if (Time.time > nextShootTime ) {
				nextShootTime += shootDuration;
        		enemy.Shoot();
     		}
    		
    	} else 
    	{
    		enemy.ChangeState(new IdleState());
    	}

    }

    public void Enter(Enemy enemy){
    	this.enemy = enemy;
    	nextShootTime = Time.time;
    }

    public void Exit(){

    }

    public void OnTriggerEnter(Collider2D other){
        if (other.tag == "FlipEdge")
        {
            enemy.Flip();
        } 

        if (enemy.Target != null)
        {
            if (other.tag == "JumpEdgeRight" && enemy.facingRight)
            {
                if (enemy.Target.transform.position.y>enemy.transform.position.y) enemy.Jump();
            } else if (other.tag == "JumpEdgeLeft" && !enemy.facingRight)
            {
                if (enemy.Target.transform.position.y> enemy.transform.position.y) enemy.Jump();
            }
        } else enemy.ChangeState(new IdleState());
    }

    void Ranged()
    {
        enemy.anim.SetBool("isGrounded", enemy.IsGrounded());
        if (enemy.IsGrounded())
        {
            enemy.anim.SetFloat("xVel", enemy.GetComponent<Enemy>().maxSpeedWalk);
        } 
        
        enemy.anim.SetBool("isDown", false);

    }


}
