﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollision : MonoBehaviour {


	public string[] tags;

	GameObject[] noCollideObjects;

	// Use this for initialization
	void Start () {
		foreach (string tag in tags)
		{
			noCollideObjects = GameObject.FindGameObjectsWithTag(tag);
			foreach (GameObject noCollideObject in noCollideObjects)
        		{
        			Physics2D.IgnoreCollision(GetComponent<Collider2D>(), noCollideObject.GetComponent<Collider2D>(), true);
        		}
		}
	}
}
