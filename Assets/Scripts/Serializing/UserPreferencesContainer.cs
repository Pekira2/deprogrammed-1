using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
 
[XmlRoot("UserPreferencesCollection")]
public class UserPreferencesContainer
{
    [XmlArray("UserPreferences")]
    //public UserPreference[] UserPreferences;
    public List<UserPreference> userPreferences = new List<UserPreference>();
   
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(UserPreferencesContainer));
        using(var stream = new FileStream(path, FileMode.Create))
        {
            var xmlWriter = new XmlTextWriter(stream, Encoding.UTF8);
            serializer.Serialize(xmlWriter, this);
        }
    }
   
    public static UserPreferencesContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(UserPreferencesContainer));
        using(var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as UserPreferencesContainer;
        }
    }
}