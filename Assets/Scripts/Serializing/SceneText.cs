using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

public class SceneText
{
	public string who;
	public string type;
	public string NPCtype;
	public string enemyType;
	public string playerPos;
	public string textId;
	public string isChecked;
}