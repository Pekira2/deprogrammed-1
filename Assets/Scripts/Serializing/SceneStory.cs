using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
 
public class SceneStory
{
	public string sceneName;
	[XmlElement("SceneText")]
    public List<SceneText> sceneTexts = new List<SceneText>();
}