using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
 
public class UserGameProgress
{
	public string gameTextCount;
    public string gameName;
	public string saveNow;
	public string scene;
	public string health;
	public string colors;
}