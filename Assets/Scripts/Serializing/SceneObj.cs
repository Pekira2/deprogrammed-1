using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
 
public class SceneObj
{
	public string sceneName;
	public string width;
	public string heigth;
	public string line19;
	public string line18;
	public string line17;
	public string line16;
	public string line15;
	public string line14;
	public string line13;
	public string line12;
	public string line11;
	public string line10;
	public string line09;
	public string line08;
	public string line07;
	public string line06;
	public string line05;
	public string line04;
	public string line03;
	public string line02;
	public string line01;
	public string line00;
}