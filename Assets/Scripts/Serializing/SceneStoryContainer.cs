using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
 
[XmlRoot("SceneStoryCollection")]
public class SceneStoryContainer
{
    [XmlArray("Scenes")]
    
    public List<SceneStory> sceneStories = new List<SceneStory>();
   
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(SceneStoryContainer));
        using(var stream = new FileStream(path, FileMode.Create))
        {
            var xmlWriter = new XmlTextWriter(stream, Encoding.UTF8);
            serializer.Serialize(xmlWriter, this);
        }
    }
   
    public static SceneStoryContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(SceneStoryContainer));
        using(var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as SceneStoryContainer;
        }
    }
}