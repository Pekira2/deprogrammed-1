using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class moveOnActivator : MonoBehaviour {

    bool pause = false;
    
    public GameObject cam;
    public GameObject player;
    public float playerRadius = 0.5f;
    public LayerMask whatIsPlayer;
    private string nextSceneName;
    
    void Update()
    {
         if (!pause) {
            if (Physics2D.OverlapCircle(transform.position, playerRadius, whatIsPlayer) ) 
            {
                 Invoke("LoadScene", 2);
                 SaveGameProgress();
                 cam.GetComponent<Camera>().orthographicSize = 1;
                 player.GetComponent<Animator>().SetBool("isMovingOn", true);
                 NotificationCenter.DefaultCenter().PostNotification(this, "Stop");
            }
        }
    }

    void LoadScene ()
    {
        foreach (SceneSeq sceneSeq in GameManager.gameManager.sceneSequence.SceneSequences) {
            if (sceneSeq.sceneName == SceneManager.GetActiveScene().name)
            {
                SceneManager.LoadScene(sceneSeq.nextSceneName);
                break;
            }
        }
    }

    void SaveGameProgress()
    { 
        foreach (SceneSeq sceneSeq in GameManager.gameManager.sceneSequence.SceneSequences) {
            if (sceneSeq.sceneName == SceneManager.GetActiveScene().name)
            {
                nextSceneName = sceneSeq.nextSceneName;
                break;
            }
        }
        if (GameManager.gameManager.currentUserGameProgress!=null) {
            GameManager.gameManager.currentUserGameProgress.colors = GameManager.gameManager.notSavedGameProgress.colors;
            foreach (UserGameProgress userGameProgress in GameManager.gameManager.userGameProgressCollection.usersGameProgress) {
                if (userGameProgress.gameName==GameManager.gameManager.currentUserGameProgress.gameName) {
                    userGameProgress.scene = nextSceneName;                
                    GameManager.gameManager.currentUserGameProgress = userGameProgress;
                    break;
                }
            }
        }
        GameManager.gameManager.SaveUserGameProgress();
    }
}
