using UnityEngine;
using System.Collections;

public class instructionsActivator : MonoBehaviour {

    bool pause = false;
    
    public GameObject actInstruction;

    public float playerRadius = 4.5f;
    public LayerMask whatIsPlayer;
    GameObject cloneInstruction;
    public float timeInstruction = 10.0f;
    private bool oneTime = false;
    
    void Update()
    {
         if (!pause && !oneTime) {
            if (Physics2D.OverlapCircle(transform.position, playerRadius, whatIsPlayer) ) 
            {
                cloneInstruction = (GameObject)  Instantiate(actInstruction, new Vector3(transform.position.x-3.0f, transform.position.y, transform.position.z),
                    Quaternion.identity) as GameObject;
                Destroy(cloneInstruction.gameObject, timeInstruction);
                oneTime = true;
            }
        }
    }
}
