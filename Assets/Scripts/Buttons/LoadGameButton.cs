﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadGameButton : MonoBehaviour
{
    void OnMouseDown()
    {  
            SceneManager.LoadScene("0LOADGAME");
            GetComponent<AudioSource>().Play();
    }    
}
