using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GameManager : MonoBehaviour
{

    public Texture[] firstBack;
    public Texture[] secondBack;

    public static GameManager gameManager;

    public string newGameName;

    private string userPreferencesFileName = "userPreferences.xml";
    private string userGameProgressFileName = "userGameProgress.xml";
    private string sceneFileName = "sceneStruc.xml";
    private string pageFileName = "pageStruc.xml";
    private string sceneSeqFileName = "sceneSeq.xml";
    private string sceneStoryFileName = "sceneStory.xml";

    public UserPreferencesContainer userPreferencesCollection;
    public UserGameProgressContainer userGameProgressCollection;
    
    public UserGameProgress currentUserGameProgress;
    public UserGameProgress notSavedGameProgress;

    public TextContainer textCollection;
    public SceneContainer sceneCollection;
    public PageContainer pageCollection;
    public SceneSequence sceneSequence;
    public SceneStoryContainer sceneStoryCollection;
    
    public bool pause = false;

    private UserPreference userPreference;

    public int level;



    public GameObject backObj;

    public string language;

    //public string path;

    public string PersistentDataPath(string filename) {
        string result = Application.persistentDataPath + "/" + filename;
        return result.Replace('/', '\\');
    }

    public string DataPath(string filename) {
        string result = Application.dataPath + "/StreamingAssets/" + filename;
        return result.Replace('/', '\\');
    }

    void Awake()
    {
        if (gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (gameManager != this)
        {
            Destroy(gameObject);
        }
        Load();
    }

    void Start()
    {
     
     
    }

    public void SaveUserGameProgress()
    {
        if (userGameProgressCollection==null) userGameProgressCollection = new UserGameProgressContainer(); 
        userGameProgressCollection.Save(PersistentDataPath(userGameProgressFileName));
    }

    public void SaveUserPreferences()
    {
        if(userPreferencesCollection==null)
        {
            userPreferencesCollection = new UserPreferencesContainer();
            userPreference = new UserPreference();
            userPreference.id = "language";
            userPreference.value = "900";
            userPreferencesCollection.userPreferences.Add(userPreference);
        }
        userPreferencesCollection.Save(PersistentDataPath(userPreferencesFileName));
    }

    public void Load()
    {

        //path = PersistentDataPath(userPreferencesFileName);
        if(File.Exists(PersistentDataPath(userPreferencesFileName))) {
            userPreferencesCollection = UserPreferencesContainer.Load(PersistentDataPath(userPreferencesFileName));
            foreach (UserPreference userPreference in userPreferencesCollection.userPreferences) {  
                if (userPreference.id=="language") language = userPreference.value;
            }
        } else SaveUserPreferences();
        
        //path = PersistentDataPath(userGameProgressFileName);
        if(File.Exists(PersistentDataPath(userGameProgressFileName))) {
            userGameProgressCollection = UserGameProgressContainer.Load(PersistentDataPath(userGameProgressFileName));
        } else SaveUserGameProgress();
                
        string textFileName = "text.xml";
        if (!String.IsNullOrEmpty(language)) textFileName = string.Concat(language, textFileName);
        else textFileName = string.Concat("900", textFileName);
        //path = DataPath(textFileName);
        if(File.Exists(DataPath(textFileName))) {
            textCollection = TextContainer.Load(DataPath(textFileName));
        }
        
        //path = DataPath(sceneFileName);
        if(File.Exists(DataPath(sceneFileName))) {
            sceneCollection = SceneContainer.Load(DataPath(sceneFileName));
        }
       
        if(File.Exists(DataPath(pageFileName))) {
            pageCollection = PageContainer.Load(DataPath(pageFileName));
        } 

        //path = DataPath(sceneSeqFileName);
        if(File.Exists(DataPath(sceneSeqFileName))) {
            sceneSequence = SceneSequence.Load(DataPath(sceneSeqFileName));
        }
        
        if(File.Exists(DataPath(sceneStoryFileName))) {
            sceneStoryCollection = SceneStoryContainer.Load(DataPath(sceneStoryFileName));
            /*foreach (SceneStory sceneStory in sceneStoryCollection.sceneStories) {  
                foreach (SceneText sceneText in sceneStory.sceneTexts) {
                    Debug.Log(sceneText.textId);  
                }    
            }*/
        }

    }
}