﻿using UnityEngine;
using System.Collections;

public class objTimeIntervalGenerator : MonoBehaviour {

	
	public GameObject[] obj;
	public float minTime = 2.25f;
    public float maxTime = 10.5f;
    private GameObject player;

    GameObject cloneObj;
	
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		Generate();
	}
	
	
	void Generate () {
		if (!GameManager.gameManager.pause && player !=null) {

			cloneObj = (GameObject)  Instantiate(obj[Random.Range(0, obj.Length)], transform.position, Quaternion.identity) as GameObject;            		
		 
		 	if (cloneObj.GetComponent<objController>()!=null)
		 	{
				cloneObj.GetComponent<objController>().player = player.GetComponent<Transform>();
				cloneObj.GetComponent<objController>().groundCheckPlayer = player.GetComponent<playerController>().groundCheck;
			}

        	Invoke("Generate", Random.Range(minTime, maxTime));
    	}
	}
}
