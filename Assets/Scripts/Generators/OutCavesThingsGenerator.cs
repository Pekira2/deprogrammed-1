﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutCavesThingsGenerator : MonoBehaviour {

	public GameObject[] thingsToGenerate;
	public float xOffset = 0.3f;
	GameObject clone;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < Random.Range(0, 3); i++)
			if (Random.Range(-1.0f, 1.0f) > 0)
			{
				float xOff = Random.Range(-xOffset, xOffset);
				GameObject clone = Instantiate(thingsToGenerate[Random.Range(0, thingsToGenerate.Length)], 
					new Vector3(transform.position.x+xOff, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
				if (Random.Range(-1.0f, 1.0f) > 0) 
				{
					Vector3 theScale = transform.localScale;
        			theScale.x *= -1;
        			clone.transform.localScale = theScale;
				}
			}
	}
	

}
