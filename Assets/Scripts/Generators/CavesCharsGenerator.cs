﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CavesCharsGenerator : MonoBehaviour {

	public GameObject[] thingsToGenerate;
	public float xOffset = 0.3f;
	GameObject clone;
	int i;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < Random.Range(0, 3); i++)
			if (Random.Range(-1.0f, 1.0f) > 0)
			{
				float xOff = Random.Range(-xOffset, xOffset);
				i = Random.Range(0, thingsToGenerate.Length);
				GameObject clone = Instantiate(thingsToGenerate[i], 
					new Vector3(transform.position.x+xOff, transform.position.y, thingsToGenerate[i].transform.position.z), Quaternion.identity) as GameObject;
			}
	}
	

}
