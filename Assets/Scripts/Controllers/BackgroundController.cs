﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackgroundController : MonoBehaviour {

	public int backNumber;
	Shader shader;
	public int level;

	void Start () {

		if (level == 999) level = GameManager.gameManager.level;

		if (backNumber == 1)
			if (GameManager.gameManager.firstBack[level] != null)
			{
				GetComponent<MeshRenderer>().materials[0] = new Material(Shader.Find("Unlit/Transparent"));
				GetComponent<MeshRenderer>().materials[0].mainTexture = GameManager.gameManager.firstBack[level];
			}
			else Destroy(gameObject, 0);

		else if (backNumber == 2)
			if (GameManager.gameManager.secondBack[level] != null)
			{
				GetComponent<MeshRenderer>().materials[0] = new Material(Shader.Find("Unlit/Transparent"));
				GetComponent<MeshRenderer>().materials[0].mainTexture = GameManager.gameManager.secondBack[level];
			}
			else Destroy(gameObject, 0);
	}
}
