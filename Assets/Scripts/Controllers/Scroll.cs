﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour {

	
	public float velocidad = 0f;
	private float tiempoInicio = 0f;

	void Update () {
		if(GameManager.gameManager.pause == false)
			GetComponent<Renderer>().material.mainTextureOffset = new Vector2(((Time.time - tiempoInicio) * velocidad) % 1, 0);
	}
}
