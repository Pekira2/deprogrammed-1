using UnityEngine;
using System;
using System.Collections;

public class enemiesCounterTextController : MonoBehaviour {

  private GameObject[] enemies;
  int count = 0;
  int iniCount;

	/*void Start()
    {        
    	if (GameManager.gameManager.currentUserGameProgress!=null) {
          GameManager.gameManager.notSavedGameProgress = new UserGameProgress();
    	   GameManager.gameManager.notSavedGameProgress.colors = GameManager.gameManager.currentUserGameProgress.colors;
      }
    }*/

    void FixedUpdate()
    {

      	if (iniCount==0)	
      	{
      		enemies = GameObject.FindGameObjectsWithTag("Enemy");
        	iniCount = enemies.Length;
    	}
    	
      count = 0;
      foreach (GameObject enemy in enemies)
      {
        if (enemy.GetComponent<Enemy>().isDead) 
          count += 1;
      }
      transform.GetComponent<TextMesh>().text = string.Concat(count.ToString()+"/"+iniCount.ToString());
    } 

}
