using UnityEngine;
using System.Collections;
using System;

public class gameObjectFollower : MonoBehaviour
{

    public Transform gameObj;
    public float xOffset = 0;
    public float yOffset = 4f;

    void Update()
    {
        if (!GameManager.gameManager.pause) {
            if ( gameObj != null) {
                transform.position = new Vector3(gameObj.position.x + xOffset, gameObj.position.y+yOffset, transform.position.z);
            }
        }
    }
}