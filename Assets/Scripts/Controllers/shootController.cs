using UnityEngine;
using System.Collections;

public class shootController : MonoBehaviour {

    public float speed = 5f;
    public bool facingRight;
    public float shootTime = 1;        
    
 	void Start()
    {
        if (facingRight) GetComponent<Rigidbody2D>().velocity = new Vector3 (speed, 0, 0);
        else GetComponent<Rigidbody2D>().velocity = new Vector3 (-speed, 0, 0);
    }

 	void Update()
    {
        Destroy();
    }

    void Destroy()
    {
        if (Time.deltaTime>shootTime)   GameObject.Destroy(transform.gameObject,0);
        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x)<0.01 || Mathf.Abs(GetComponent<Rigidbody2D>().velocity.y)>0.01)
        {
            GameObject.Destroy(transform.gameObject,0);
        } 
    }	
}
