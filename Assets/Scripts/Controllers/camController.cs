using UnityEngine;
using System.Collections;

public class camController : MonoBehaviour {

    public float minSize = 1.0f;
    public float maxSize = 9.0f;
    
    void Update()
    {
         //if (!GameManager.gameManager.pause) {
            var d = Input.GetAxis("Mouse ScrollWheel");
            if (d > 0f && GetComponent<Camera>().orthographicSize > minSize) {
                    // scroll up
                    GetComponent<Camera>().orthographicSize -= 1f;
            }
            
            if (d < 0f && GetComponent<Camera>().orthographicSize < maxSize) {
                    // scroll down
                    GetComponent<Camera>().orthographicSize += 1f;
            }
            //Debug.Log(GetComponent<Camera>().orthographicSize);
        //}
    }
}
