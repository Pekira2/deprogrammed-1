using UnityEngine;
using System.Collections;

public class hPlatformController : MonoBehaviour {

    public float xVel = 0.1f;
    public float maxXOffset = 0.4f;
    float xIni;
    int flip = 1;

    private GameObject groundCheckPlayer;
    private GameObject player;

    void Start()
    {
        xIni = GetComponent<Transform>().position.x;
        groundCheckPlayer = GameObject.FindGameObjectWithTag("groundCheckPlayer");
        player = GameObject.FindGameObjectWithTag("Player");
    }
    
    void Update()
    {
        if (!GameManager.gameManager.pause) {
            
            if ( Mathf.Abs(GetComponent<Transform>().position.x-xIni) > maxXOffset )
                flip = (-1) * flip;
            
            GetComponent<Transform>().position = new Vector3(GetComponent<Transform>().position.x + flip*xVel, GetComponent<Transform>().position.y, GetComponent<Transform>().position.z);  
            
            if (this!=null && groundCheckPlayer!=null)
            if ( (  Mathf.Abs( GetComponent<Transform>().position.x - groundCheckPlayer.GetComponent<Transform>().position.x)
                                    <= GetComponent<BoxCollider2D>().size.x )  &&
                    (  Mathf.Abs( GetComponent<Transform>().position.y - groundCheckPlayer.GetComponent<Transform>().position.y)
                                    <= GetComponent<BoxCollider2D>().size.y ) )               
                    player.GetComponent<Transform>().parent = GetComponent<Transform>();
        
        } else if (GetComponent<AudioSource>().isPlaying) GetComponent<AudioSource>().Stop();
    }

}
