using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour {

    public Transform shootTransform;
    

    public GameObject characterInfoPrefab;
    GameObject info;
    bool canGo = false;
    bool going = false;
    string goScene;

    bool canRegister = false;
    //bool registering = false;
    GameObject enemyToRegister;

    bool canGetHealth = false;
    
    public float maxSpeedRotate = 10f;
    public float maxSpeedWalk = 5f;
    float maxSpeedWalk2;
    bool facingRight = true;
    

    Animator anim;

    bool grounded = true;
    bool down = false;
    bool climbing = false;
    

    public Transform groundCheck;

    public LayerMask whatIsGround;
    public float groundRadius = 0.3f;

    public LayerMask whatCanBeClimbed;
    public float climbRadius = 0.3f;

    public float jumpForce = 700f;
    int jumpCounter = 0;
    public int maxJumpCounter = 2;

    public float climbForce = 350f;
    int climbCounter = 0;
    public int maxClimbCounter = 2;
    
    float xVel;
    float yVel;

    //public Transform groundCheckPlayer;
    public AudioSource jumpSound;

    public GameObject shootPrefab;

    public int lifes = 3;
    bool isDead;

    public int initialAmmo = 100;
    public int ammo;

    public LayerMask layerEnemy;
    public LayerMask layerCanExplote;


    GameObject cloneBackObj;

    void Start()
    {
        anim = this.GetComponent<Animator>();    
        info = (GameObject)  Instantiate(characterInfoPrefab, new Vector3 (GetComponent<Transform>().position.x, GetComponent<Transform>().position.y+0.3f, GetComponent<Transform>().position.z-1), Quaternion.identity) as GameObject;
        info.GetComponent<characterTextController>().character = transform.gameObject;
        ammo = initialAmmo;  
        info.transform.GetComponent<TextMesh>().text =  string.Concat(lifes+"/"+ammo);
    }

    public void Kill()
    {
        lifes = 0;
    }

    void Update()
    {
        if (lifes == 0) isDead = true;

        if (!isDead)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                maxSpeedWalk2 = maxSpeedWalk*2f;
            } else
            {
                maxSpeedWalk2 = maxSpeedWalk;
            }

            if (!down)
            {
                xVel = Input.GetAxis("Horizontal") * maxSpeedWalk2;
            } else
                xVel = Input.GetAxis("Horizontal") * maxSpeedRotate;
             
            grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
            if (grounded) jumpCounter=0;

            climbing = canClimb();
            if (!climbing) climbCounter=0;
          
            GetComponent<Rigidbody2D>().velocity = new Vector2(xVel , GetComponent<Rigidbody2D>().velocity.y);
            
            if(Input.GetKeyDown(KeyCode.W) && !climbing)
            {
                if (!down && grounded) {
                    if (jumpCounter < maxJumpCounter)
                    {
                        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
                        //jumpSound.Play();
                    }
                } else
                    down = !down;
            }

            if(Input.GetKeyDown(KeyCode.W) && climbing )
            {
                if (climbCounter < maxClimbCounter)
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, climbForce));
            }

            if( Input.GetKeyDown(KeyCode.S) && grounded)
            {
                down = true;
            }

            if( Input.GetKeyDown(KeyCode.E) && canGo)
            {
                going = true;
                Invoke("GoScene",1);
            }

            if( Input.GetKeyDown(KeyCode.E) && canRegister)
            {
                ammo += enemyToRegister.GetComponent<Enemy>().extraAmmo;
                enemyToRegister.GetComponent<Enemy>().extraAmmo = 0;
                enemyToRegister.GetComponent<Enemy>().isVoid = true;
                enemyToRegister.GetComponent<Enemy>().info.transform.GetComponent<TextMesh>().text = "";
                info.transform.GetComponent<TextMesh>().text =  string.Concat(lifes+"/"+ammo);
                enemyToRegister = null;
                canRegister = false;
            }

            if( Input.GetKeyDown(KeyCode.E) && canGetHealth && lifes<10)
            {
                lifes += 1;
            } else if (canGetHealth && lifes==10)
            {
                canGetHealth = false;
                info.transform.GetComponent<TextMesh>().text =  string.Concat(lifes+"/"+ammo);
            }

            if (Input.GetMouseButtonDown (0)) {
                Shoot();
            }

            if (xVel > 0 && !facingRight)
                Flip();
            else if (xVel < 0 && facingRight)
                Flip();
                       
        } else
        {
            xVel = 0;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0 , 0);
            info.transform.GetComponent<TextMesh>().text = "";
            Invoke("LoadScene", 5);
        }

        anim.SetFloat("xVel", Mathf.Abs(xVel));  
        anim.SetBool("isGrounded", grounded);
        anim.SetBool("isDown", down);
        anim.SetBool("isDead", isDead);
        anim.SetBool("isGrapping", climbing);
        anim.SetBool("isGoing", going);
      
    }


    void Flip ()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void Shoot ()
    {
        #pragma warning disable 0219
        int xOff = 0;
        #pragma warning restore 0219
        if (ammo>0)
        {
            if (facingRight) xOff = 2;
            else xOff = -2;
            GameObject shoot = Instantiate(shootPrefab, new Vector3(shootTransform.position.x, shootTransform.position.y, shootTransform.position.z), Quaternion.identity) as GameObject;
            shoot.GetComponent<shootController>().facingRight = facingRight;
            if (!facingRight) shoot.transform.localScale *= -1;
            ammo-=1;
            info.transform.GetComponent<TextMesh>().text = string.Concat(lifes+"/"+ammo);
            hit();
        }
    }

    void hit()
    {
     
        Vector3 targetPosition;
        if (facingRight) targetPosition = new Vector3 (transform.position.x+10, transform.position.y, transform.position.z);
        else             targetPosition = new Vector3 (transform.position.x-10, transform.position.y, transform.position.z);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, targetPosition - transform.position,
            Vector2.Distance(targetPosition, transform.position), layerEnemy);
     
        if (hit.collider != null)
        {
            hit.transform.gameObject.GetComponent<Enemy>().ApplyDamage(1);  
            hit.transform.gameObject.GetComponent<Enemy>().Target = transform.gameObject;  
        }

        hit = Physics2D.Raycast(transform.position, targetPosition - transform.position,
            Vector2.Distance(targetPosition, transform.position), layerCanExplote);
     
        if (hit.collider != null)
        {  
            hit.transform.gameObject.GetComponent<Explosion>().Explote();
        }


    }

    bool canClimb()
    {
        Vector3 targetPosition;
        if (facingRight) targetPosition = new Vector3 (shootTransform.position.x+climbRadius, shootTransform.position.y, shootTransform.position.z);
        else             targetPosition = new Vector3 (shootTransform.position.x-climbRadius, shootTransform.position.y, shootTransform.position.z);

        RaycastHit2D hit = Physics2D.Raycast(shootTransform.position, targetPosition - shootTransform.position,
            Vector2.Distance(targetPosition, shootTransform.position), whatCanBeClimbed);
     
        return (hit.collider != null);
    }

    public void ApplyDamage(int damage)
    {
        if (lifes>0) lifes = lifes - damage;
        info.transform.GetComponent<TextMesh>().text =  string.Concat(lifes+"/"+ammo);
    }

    void LoadScene ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void OnTriggerStay2D(Collider2D other)
    {   
        if (other.tag == "Door")
        {
            info.transform.GetComponent<TextMesh>().text =  "E";
            goScene = other.GetComponent<Door>().scene;
            canGo = true;
        }

        if (other.tag == "EnemySight")
        {
            if (!other.transform.parent.gameObject.GetComponent<Enemy>().isVoid)
            {
                info.transform.GetComponent<TextMesh>().text =  "E";
                enemyToRegister = other.transform.parent.gameObject;
                canRegister = true;
            } 
        }

        if (other.tag == "Health")
        {
            info.transform.GetComponent<TextMesh>().text =  "E";
            canGetHealth = true;
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {   
        if (other.tag == "Door")
        {
            info.transform.GetComponent<TextMesh>().text =  string.Concat(lifes+"/"+ammo);
            goScene = "";
            canGo = false;
        }   

        if (other.tag == "EnemySight")
        {
            info.transform.GetComponent<TextMesh>().text =  string.Concat(lifes+"/"+ammo);
            enemyToRegister = null;
            canRegister = false;
        }
        if (other.tag == "Health")
        {
            info.transform.GetComponent<TextMesh>().text =  string.Concat(lifes+"/"+ammo);
            canGetHealth = false;
        }
    }

    /*void OnParticleCollision2D (GameObject other) 
    {
        Debug.Log("entra");
        Kill();   
    }*/

    void GoScene ()
    {
        SceneManager.LoadScene(goScene);
    }

}
