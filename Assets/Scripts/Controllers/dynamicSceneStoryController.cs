﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class dynamicSceneStoryController : MonoBehaviour {

    public float playerRadius = 1f;
    public LayerMask whatIsPlayer;
	public GameObject textDialogPrefab;
	public GameObject textThinkPrefab;
	public GameObject textNarratePrefab;
	private GameObject textPrefab;
	private GameObject posText;
	public float textTime = 5f;
	public int lineLenghtDialog = 17;
	public int lineLenghtThink = 17;
	public int lineLenghtNarrate = 31;
	private int lineLenght;

	private GameObject npc;
    private GameObject player;

    private float xOffDialog = 0f;
    private float yOffDialog = 4f;
    private float xOffThink = 1f;
    private float yOffThink = 4f;
    private float xOffNarrate = 0f;
    private float yOffNarrate = -4f;

	GameObject clonObject;

	private List<SceneStory> sceneStories = new List<SceneStory>();

	string yes = "x";
	bool allowNextText = true;

	float xText;
	float yText;
	
	void Start () {
		Initialize();
		LoadSceneStory();
	}
	
	void FixedUpdate () {

		if (npc==null || player == null) Initialize();

		//if( Input.GetKeyDown(KeyCode.E)) AllowNextText();

		foreach (SceneStory sceneStory in sceneStories) {  
			foreach (SceneText sceneText in sceneStory.sceneTexts) {
				 
                if (sceneText.isChecked != yes && allowNextText)
                
                	if (sceneText.type == "")
                		if (sceneText.playerPos == "1" || sceneText.playerPos == "2" || sceneText.playerPos == "3" || sceneText.playerPos == "4" || sceneText.playerPos == "5" || sceneText.playerPos == "6" || sceneText.playerPos == "7" || sceneText.playerPos == "8" || sceneText.playerPos == "9")
                		{
                			posText= GameObject.FindGameObjectWithTag(sceneText.playerPos);
             				if (Physics2D.OverlapCircle(posText.transform.position, playerRadius, whatIsPlayer) ) 
             				{
					    		drawText(sceneText);
							}	 
						} else
							break;

                	else if (sceneText.type == "think")
                		if (sceneText.playerPos == "1" || sceneText.playerPos == "2" || sceneText.playerPos == "3" || sceneText.playerPos == "4" || sceneText.playerPos == "5" || sceneText.playerPos == "6" || sceneText.playerPos == "7" || sceneText.playerPos == "8" || sceneText.playerPos == "9")
                		{
                			posText= GameObject.FindGameObjectWithTag(sceneText.playerPos);
             				if (Physics2D.OverlapCircle(posText.transform.position, playerRadius, whatIsPlayer) ) 
             				{
					    		drawText(sceneText);
							}	 
						} else
							break;
				
					else if (sceneText.type == "dialog")
						if(Input.GetKeyDown(KeyCode.E) && Physics2D.OverlapCircle(npc.transform.position, playerRadius, whatIsPlayer) ) 
							drawText(sceneText);
    	    }
		}
	}

	void Initialize () {

		player = GameObject.FindGameObjectWithTag("Player");
		npc    = GameObject.FindGameObjectWithTag("NPC");
	}

	void LoadSceneStory () {
		foreach (SceneStory sceneStory in GameManager.gameManager.sceneStoryCollection.sceneStories) {  
           if (sceneStory.sceneName == SceneManager.GetActiveScene().name)
           		sceneStories.Add(sceneStory);         
        }
	}

	void drawText (SceneText psceneText) {

		if (psceneText.type == "dialog")
		{
			textPrefab = textDialogPrefab;
			lineLenght = lineLenghtDialog;

			if (psceneText.who == "player")
			{	
				xText = player.transform.position.x+xOffDialog;
				yText = player.transform.position.y+yOffDialog;

			} else if (psceneText.who == "NPC")
			{	
				xText = npc.transform.position.x+xOffDialog;
				yText = npc.transform.position.y+yOffDialog;
			}

		} else if (psceneText.type == "think")
		{
			textPrefab = textThinkPrefab;
			lineLenght = lineLenghtThink;
			xText = player.transform.position.x+xOffThink;
			yText = player.transform.position.y+yOffThink;

		} else
		{
			textPrefab = textNarratePrefab;
			lineLenght = lineLenghtNarrate;
			xText = player.transform.position.x+xOffNarrate;
			yText = player.transform.position.y+yOffNarrate;
		}

		clonObject = (GameObject)  Instantiate(textPrefab, new Vector3 (xText, yText, transform.position.z), Quaternion.identity) as GameObject; 
		
		clonObject.transform.Find("text").GetComponent<textController>().textId = psceneText.textId;
		clonObject.transform.Find("text").GetComponent<textController>().lineLenght = lineLenght;
		
		if (psceneText.type == "") FixFollower(player, xOffNarrate, yOffNarrate); 
		else if (psceneText.type == "think") FixFollower(player, xOffThink, yOffThink);
		else if (psceneText.type == "dialog")
			if (psceneText.who == "player") FixFollower(player, xOffDialog, yOffDialog);
			else if (psceneText.who == "NPC") FixFollower(npc, xOffDialog, yOffDialog);
		
		

		allowNextText = false;
		Invoke("AllowNextText", textTime);
		//Destroy(clonObject, textTime);
		psceneText.isChecked = yes;
	}

	void FixFollower(GameObject pgameObj, float xOff, float yOff)
	{
		clonObject.transform.GetComponent<gameObjectFollower>().gameObj = pgameObj.transform;
		clonObject.transform.GetComponent<gameObjectFollower>().xOffset = xOff;
		clonObject.transform.GetComponent<gameObjectFollower>().yOffset = yOff;
	}

	void AllowNextText() {
		allowNextText = true;
		Destroy(clonObject);
	}
}
