using UnityEngine;
using System;
using System.Collections;

public class countColorsTextController : MonoBehaviour {

	void Start()
    {        
    	if (GameManager.gameManager.currentUserGameProgress!=null) {
          GameManager.gameManager.notSavedGameProgress = new UserGameProgress();
    	   GameManager.gameManager.notSavedGameProgress.colors = GameManager.gameManager.currentUserGameProgress.colors;
      }
    }

    void FixedUpdate()
    {
       if (GameManager.gameManager.notSavedGameProgress!=null && GameManager.gameManager.notSavedGameProgress.colors != "000") 
       	transform.GetComponent<TextMesh>().text = string.Concat(GameManager.gameManager.notSavedGameProgress.colors);
    } 

}
