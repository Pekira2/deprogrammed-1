using UnityEngine;
using System.Collections;

public class NPCWalkingState : INPCState {

    private NPC npc;
    private float walkTimer;
    private float walkDurationMin = 7;
    private float walkDurationMax = 14;
    float walkDuration;
    float boolForJump;

    public void Execute(){
        Walk();
        npc.Move();
    }

    public void Enter(NPC npc){
        this.npc = npc;
        walkDuration = Random.Range(walkDurationMin, walkDurationMax);
    }

    public void Exit(){


    }

    public void OnTriggerEnter(Collider2D other){
        if (other.tag == "FlipEdge")
        {
            npc.anim.SetFloat("xVel", -npc.anim.GetFloat("xVel"));
            npc.Flip();
        }

    }

    void Walk()
    {
        npc.anim.SetBool("isGrounded", npc.IsGrounded());
        if (npc.IsGrounded())
        {   
            npc.anim.SetFloat("xVel", npc.GetComponent<NPC>().maxSpeedWalk);
        }
        
        npc.anim.SetBool("isDown", false);
        npc.anim.SetBool("isDead", false);

        walkTimer += Time.deltaTime;

        if (walkTimer >= walkDuration)
        {
            npc.ChangeState(new NPCIdleState());
        }

    }

}
