using UnityEngine;
using System.Collections;

public class NPCIdleState : INPCState {

	private NPC npc;
	private float idleTimer;
	private float idleDurationMin = 3;
    private float idleDurationMax = 12;
    float idleDuration;
    float randomFlip;

    public void Execute(){
    	Idle();
    }

    public void Enter(NPC npc){
    		this.npc = npc;
            idleDuration = Random.Range(idleDurationMin, idleDurationMax);

    }

    public void Exit(){


    }

    public void OnTriggerEnter(Collider2D other){


    }

    void Idle()
    {
    	npc.anim.SetFloat("xVel", 0);
    	npc.anim.SetBool("isGrounded", true);
    	npc.anim.SetBool("isDown", false);

        npc.GetComponent<Rigidbody2D>().velocity = new Vector2(0 , npc.GetComponent<Rigidbody2D>().velocity.y);

    	idleTimer += Time.deltaTime;

    	if (idleTimer >= idleDuration)
    	{
    		
            randomFlip = Random.Range(-1.0f, 1.0f);
            if (randomFlip>0)
            {
                npc.Flip();
            } 
            npc.ChangeState(new NPCWalkingState());
    	}
    }


}
