using UnityEngine;
using System.Collections;

public class NPCDeadState : INPCState {

	private NPC npc;

    public void Execute(){
    	Dead();
    }

    public void Enter(NPC npc){
    		this.npc = npc;
    }

    public void Exit(){
    		
    }

    public void OnTriggerEnter(Collider2D other){

    }

    void Dead()
    {
    	npc.anim.SetBool("isDead", true);
        npc.GetComponent<Rigidbody2D>().velocity = new Vector2(0 , 0);
    }


}
