using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeLine : MonoBehaviour {

	public float speed = 0.2f;
	public float secondsFlip = 1.0f;
	bool facingRight = true;
	float flipTimer = 0;
	

	// Use this for initialization
	void Start () {
		facingRight = (Random.Range(-1.0f, 1.0f) > 0);
		speed = Random.Range(-speed, speed);
		secondsFlip = Random.Range(secondsFlip, secondsFlip*2);
	}
	
	// Update is called once per frame
	void Update () {
		
		flipTimer += Time.deltaTime;
		if (flipTimer > secondsFlip)
			{
				facingRight = !facingRight;
				flipTimer = 0;	
		} else if (facingRight)
			transform.position = new Vector3 (transform.position.x+speed, transform.position.y, transform.position.z);
		else if (!facingRight)
			transform.position = new Vector3 (transform.position.x-speed, transform.position.y, transform.position.z);
	}
}
